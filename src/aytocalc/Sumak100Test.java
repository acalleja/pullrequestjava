package aytocalc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

public class Sumak100Test {
	@Test
	void testSumak100() {
		Sumak100 calculadora= new Sumak100();
		double resultado = calculadora.calcula(10.0);
		assertEquals(110.0, resultado);
		}

}
