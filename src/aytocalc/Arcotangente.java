package aytocalc;

public class Arcotangente {

	public double calcula(double a, double b) {
		
		return Math.atan2(a, b); 
		
	}
	
}
