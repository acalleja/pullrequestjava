package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CuboTest {

	@Test
	void testElevaCuboPositivo() {
		Cubo calculadora = new Cubo();
		double resultado = calculadora.calcula(2.0);
		assertEquals(8.0,resultado);
		//fail("Not yet implemented");
	}
	@Test
	void testElevaCuboNegativo() {
		Cubo calculadora = new Cubo();
		double resultado = calculadora.calcula(-2.0);
		assertEquals(-8.0,resultado);
		//fail("Not yet implemented");
	}

}
