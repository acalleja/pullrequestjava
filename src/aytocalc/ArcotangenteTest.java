package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ArcotangenteTest {

	@Test
	void testOperacionAngel() {
		
		Arcotangente operacion=new Arcotangente();
		assertEquals(0.4636476090008061,operacion.calcula(1,2));
		
	}

}
