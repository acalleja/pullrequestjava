package aytocalc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SumaCuatroTest {

	@Test
	void testSumaCuatro() {
		SumaCuatro calculadora = new SumaCuatro();
		double resultado= calculadora.calcula(10.0);
		assertEquals(0, resultado);		
	}
}
